package com.agaevskihanada.data.controller;

import com.agaevskihanada.data.domain.Post;
import com.agaevskihanada.data.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/posts")
public class PostController {
    private PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @RequestMapping("/")
    public Iterable<Post> list(){
        return postService.list();
    }

    @RequestMapping("/byAuthor/{first}")
    public List<Post> byAuthor(@PathVariable String first){
        return postService.byAuthor(first);

    }

    @RequestMapping("/byKeyword/{keyword}")
    public List<Post> byKeyword(@PathVariable String keyword){
        return postService.byKeyword(keyword);
    }

    @RequestMapping("/active")
    public List<Post> active(){
        return postService.findActive();
    }

    @RequestMapping("/slug/{slug}")
    public Post findPostBySlug(@PathVariable String slug){
        return postService.findBySlug(slug);
    }
}
