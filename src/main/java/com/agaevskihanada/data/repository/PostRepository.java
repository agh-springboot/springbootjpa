package com.agaevskihanada.data.repository;

import com.agaevskihanada.data.domain.Post;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends CrudRepository<Post,Long> {

    List<Post> findAllByAuthorFirstName(String firstName);

    List<Post> findAllByAuthorFirstNameIgnoreCase(String firstName);

    List<Post> findAllByAuthorFirstNameAndAuthorLastName(String firstName, String lastName);


    List<Post> findAllByKeywords(String keyword);

    List<Post> findAllByActiveTrue();

    List<Post> findAllByAuthorFirstNameAndKeywordsOrderByPostedOnDesc(String firstName, String keywords);


    @Query("select p from Post p where p.slug = ?1")
    Post findPostBySlug(String slug);

    @Query("select p from Post p where p.slug = :slug")
    Post findPostBySlugNamedParam(@Param("slug") String slug);

    @Query(value = "SELECT * FROM Post where slug = :slug", nativeQuery = true)
    Post findPostBySlugNative(@Param("slug") String slug);
}
