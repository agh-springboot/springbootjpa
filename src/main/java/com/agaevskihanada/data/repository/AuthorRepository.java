package com.agaevskihanada.data.repository;

import com.agaevskihanada.data.domain.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author,Long> {
}
